// activar tooltip
$(function(){
    // indicar informacion adicional
    // cargar antes jquery y poper antes que bootstrap
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    $('.carousel').carousel({interval:2000});

    $('#contacto').on('show.bs.modal', function(e){console.log('el modal contacto se esta mostrando');
      // cambiar color
        $('#btnContacto').removeClass('btn-outline-sucess');
        $('#btnContacto').addClass('btn-primary');
        $('#btnContacto').prop('disabled',true);
    });
    $('#contacto').on('shown.bs.modal', function(e){console.log('el modal contacto se mostro');});

    $('#contacto').on('hide.bs.modal', function(e){console.log('el modal contacto se oculta');});
    $('#contacto').on('hidden.bs.modal', function(e){console.log('el modal contacto se oculto');
        $('#btnContacto').prop('disabled',false);
    });
});